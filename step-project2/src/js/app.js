const burgerIcon = document.querySelector(".burger-icon");
const bugerIconLines= document.querySelectorAll(".burger-icon__line");
const menu = document.querySelector(".header-navbar__list");

const burgerMenu = () => burgerIcon.addEventListener("click", (event) =>{
    bugerIconLines.forEach((element) => {
        element.classList.toggle("burger-icon__line--active");
    })
menu.classList.toggle("header-navbar__list--active");
})

const burgerMenuFocus = () => {
    document.addEventListener("click", (event) => {
        let target = event.target;
        let headerMenu = target == menu || menu.contains(target);
        let burger = target == burgerIcon || target.closest(".burger-icon");
        let headerMenuActive = menu.classList.contains('header-navbar__list--active');
    
        if (!headerMenu && !burger && headerMenuActive) {
            menu.classList.toggle("header-navbar__list--active");
            bugerIconLines.forEach((element) => {
                element.classList.toggle("burger-icon__line--active");
            })
        
        }
    })
}

burgerMenuFocus()

burgerMenu()
