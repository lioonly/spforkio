## Щигров Лев

* Блок `Revolutionary Editor`;
* Секція `Here is what you get`;
* Секція `Fork Subscription Pricing`.
* Шапка сайту з верхнім меню;
* Секція `People Are Talking About Fork`;
    
## Використані технології

* HTML/CSS;
* JS;
* SCSS;
* Git
* Gulp;
